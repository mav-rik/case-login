module.exports = [
    (value) => {
        let error = "Passwords must include one increasing straight of at least three letters, like abc , cde , fgh ,\n" +
            "and so on, up to xyz . They cannot skip letters; acd doesn't count.";
        let passed = false;
        let code;
        let prevCode;
        let deltas = [];
        for (let i = 0; i < value.length; i++) {
            code = value.charCodeAt(i);
            if (i > 0) {
                deltas.push(code-prevCode);
            }
            prevCode = code;
        }

        for (let i = 1; i < deltas.length; i++) {
            if (deltas[i] === 1 && deltas[i] === deltas[i-1]) {
                passed = true;
                break;
            }
        }


        return passed ? null : error;
    },
    (value) => {
        let error = "Passwords may not contain the letters i, O, or l, as these letters can be mistaken for other characters\n" +
            "and are therefore confusing.";

        return /^[^iOl]*$/g.test(value) ? null : error;
    },
    (value) => {
        let error = "Passwords must contain at least two non-overlapping pairs of letters, like aa, bb, or cc.";

        let pairs = {};

        for (let i = 1; i < value.length; i++) {
            if (value[i] === value[i-1]) {
                pairs[value[i]] = true;
            }
        }

        let pairsCount = 0;
        for (var p in pairs) {
            pairsCount++;
        }

        return pairsCount >= 2 ? null : error;
    },
    (value) => {
        let error = "Passwords cannot be longer than 32 characters.";

        return value.length > 32 ? error : null;
    },
    (value) => {
        let error = "Passwords can only contain lower case alphabetic characters.";

        return /^[a-z]*$/g.test(value) ? null : error;
    }
];