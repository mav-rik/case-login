const fs = require('fs')
let files = {}

module.exports = function getFile(name) {
    if (!files[name]) {
        files[name] = new Promise((resolve, reject)=>{
            fs.readFile('./static/'+name, function read(err, data) {
                if (err) {
                    reject(err);
                }
                resolve(data.toString());
            });
        })
    }
    return files[name];
}