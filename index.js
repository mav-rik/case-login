'use strict';
const koa     = require('koa'),
    router  = require('koa-router')(),
    bodyParser = require('koa-bodyparser'),
    getFile = require('./files'),
    session = require('koa-session'),
    rules = require('./rules');


let app = new koa();
app.use(bodyParser());

app.keys = ['some secret hurr'];

const CONFIG = {
    key: 'koa:sess',
    maxAge: 86400000,
    autoCommit: true,
    overwrite: true,
    httpOnly: true,
    signed: true,
    rolling: false,
    renew: false
};

app.use(session(CONFIG, app));

router.get('/', async (ctx) => {
    let file = "";

    if (ctx.session.loggedIn) {
        file = await getFile('loggedIn.html');
        file = file.replace("%username%", ctx.session.name);
    } else {
        file = await getFile('index.html');
        file = file.replace("%error_text%", "");
    }

    ctx.status = 200;
    ctx.body = file;
});

router.get('/logout', (ctx) => {
    ctx.session.loggedIn = false;
    ctx.redirect('/');
});

router.post('/', async (ctx) => {
    let file;
    let error = null;
    for (let i = 0; i < rules.length; i++) {
        const rule = rules[i];
        if (error = rule(ctx.request.body.psw)) {
            break;
        }
    }
    if (error) {
        file = await getFile('index.html');
        file = file.replace("%error_text%", error);
        file = file.replace("value=\"\"", "value=\"" + ctx.request.body.uname + "\"");
    } else {
        ctx.session.loggedIn = true;
        ctx.session.name = ctx.request.body.uname;
        file = await getFile('loggedIn.html');
        file = file.replace("%username%", ctx.session.name);
    }

    ctx.body = file;
});

app.use(router.routes());

console.log("http://localhost:3000");
app.listen(3000);








